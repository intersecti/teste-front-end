### Construa uma aplicação javascript que utilize a API do Twitter: 

#### Requisitos da aplicação
* Autenticação do twitter, 
* Carregue as ultimas 10 postagens da sua timeline;
* Efetue um post de atualização(tweet);
* O código deve ser publicado no Github (você pode nomear a sua aplicação como quiser);
* O código JS deve conter testes automatizados, na **INTERSECTI** utilizamos Jasmine;
* O Aplicativo deve ter o CSS isolado e deve sobreviver múltiplos navegadores (Chrome, FF, Safari) e outros softwares alternativos (IE8+);
* O formulário deve fornecer feedback para o usuário caso haja um erro na gravação do registro.

#### **Bônus**:
* AngularJS ou outros;
* Responsivo;
* less/sass;
* bower;
* grunt/gulp.

Qualquer dúvida, fique à vontade para perguntar no nelson@intersecti.com
Muito obrigado desde já. Boa sorte!